import sqlite3
import os


class DBAdapter(object):

    def __init__(self, db_name):
        self.db_name = db_name
        if not os.path.exists(db_name):
            self.conn = sqlite3.connect(db_name)
            self.__setup()
        else:
            self.conn = sqlite3.connect(db_name)

    def __setup(self):
        query = ''
        for line in open('../sql/dump.sql'):
            query = query + line
            if line.count(';'):
                self.conn.execute(query)
                query = ''
        self.conn.commit()

    # TODO: доделать все методы
    # def add_item(self):
    #     self.conn.execute('INSERT INTO person(user_id, location_x, location_y) VALUES (0, 0.0, 0.0)')
    #     self.conn.commit()
    # def delete_item(self):
    # def get_items(self):
    #     return [x for x in self.conn.execute('SELECT * FROM person')]
