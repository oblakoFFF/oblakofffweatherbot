from src import ROOT
from telebot.types import Message
from src.modules.keyboard import markup


@ROOT.message_handler(commands=['start'])
def index(message: Message):
    ROOT.send_message(message.chat.id, "Hello, its Weather Bot!!", reply_markup=markup['start'])
