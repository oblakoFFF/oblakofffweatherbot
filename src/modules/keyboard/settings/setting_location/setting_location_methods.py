from src import ROOT
from ... import markup, markup_keys
from telebot.types import Message


@ROOT.message_handler(func=lambda message: message.text == markup_keys['Settings']['Location']['Back to settings']['Caption'])
def select_back_to_settings(message: Message):
    ROOT.send_message(message.chat.id, 'Settings:', reply_markup=markup['settings'])


@ROOT.message_handler(func=lambda message: message.text == markup_keys['Settings']['Location']['Share Location']['Caption'])
def select_share_location(message: Message):
    pass


@ROOT.message_handler(func=lambda message: message.text == markup_keys['Settings']['Location']['Input city']['Caption'])
def select_share_location(message: Message):
    pass
