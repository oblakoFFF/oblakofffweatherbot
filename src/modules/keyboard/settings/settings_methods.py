from src import ROOT
from .. import markup, markup_keys
from telebot.types import Message


@ROOT.message_handler(func=lambda message: message.text == markup_keys['Settings']['Back to main']['Caption'])
def select_back_to_main(message: Message):
    ROOT.send_message(message.chat.id, 'Main:', reply_markup=markup['start'])


@ROOT.message_handler(func=lambda message: message.text == markup_keys['Settings']['Location']['Caption'])
def select_location(message: Message):
    ROOT.send_message(message.chat.id, 'Location:', reply_markup=markup['setting_location'])
