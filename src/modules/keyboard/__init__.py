from telebot.types import ReplyKeyboardMarkup, KeyboardButton

markup_keys = {
    'Now': {
        'Caption': 'Now'
    },
    'Settings': {
        'Caption': 'Settings',
        'Back to main': {
            'Caption': 'Back to main'
        },
        'Location': {
            'Caption': 'Location',
            'Back to settings': {
                'Caption': 'Back to settings'
            },
            'Share Location': {
                'Caption': 'Share Location'
            },
            'Input city': {
                'Caption': 'Input city'
            }
        }
    },
}


markup = {
    'start': ReplyKeyboardMarkup(resize_keyboard=True).row(
        markup_keys['Now']['Caption'],
        markup_keys['Settings']['Caption']
    ),
    'settings': ReplyKeyboardMarkup(resize_keyboard=True).row(
        markup_keys['Settings']['Back to main']['Caption']
    ).row(
        markup_keys['Settings']['Location']['Caption']
    ),
    'setting_location': ReplyKeyboardMarkup(resize_keyboard=True).row(
        markup_keys['Settings']['Location']['Back to settings']['Caption']
    ).row(
        markup_keys['Settings']['Location']['Share Location']['Caption'],
    ).row(
        markup_keys['Settings']['Location']['Input city']['Caption'],
    )
}
