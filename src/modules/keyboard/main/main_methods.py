from src import ROOT
from .. import markup_keys, markup
from telebot.types import Message
import pprint


@ROOT.message_handler(func=lambda message: message.text == markup_keys['Now']['Caption'])
def select_now(message: Message):
    ROOT.send_message(message.chat.id, 'Weather now:')


@ROOT.message_handler(func=lambda message: message.text == markup_keys['Settings']['Caption'])
def select_settings(message: Message):
    ROOT.send_message(message.chat.id, 'Settings:', reply_markup=markup['settings'])
