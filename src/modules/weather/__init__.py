TOKEN_WEATHER = "0748bab9d3c02940dbcade860b751451"
URL_WEATHER = f"https://api.openweathermap.org/data/2.5/weather?APPID={TOKEN_WEATHER}&"
WEEKNAME = {
    0: 'Monday',
    1: 'Tuesday',
    2: 'Wednesday',
    3: 'Thursday',
    4: 'Friday',
    5: 'Saturday',
    6: 'Sunday'
}
