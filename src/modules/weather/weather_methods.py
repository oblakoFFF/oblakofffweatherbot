from src import ROOT
from src.modules.keyboard import markup_keys
from . import *
import requests
import time
import datetime
from telebot.types import Message


@ROOT.message_handler(func=lambda message: True)
def sendWeather(message: Message):
    response = requests.get(URL_WEATHER, params={
        'type': 'link%2C accurate',
        'units': 'imperial%2C metric',
        'q': message.text
    }).json()
    if response['cod'] == 200:
        data_result = {
            'City': response['name'],
            'Country': response['sys']['country'],
            'Weather': str(round(float(response['main']['temp']) - 273.15, 1)).rstrip('0').rstrip('.'),
            'Local_time': datetime.datetime.strptime(time.ctime(response['dt']), "%a %b %d %H:%M:%S %Y")
        }
        result = '<b>Now, {} {}/{}/{}</b>\n' \
                 'City: {City}, {Country}\n' \
                 'Weather: {Weather}℃\n'\
            .format(WEEKNAME[data_result['Local_time'].weekday()],
                    data_result['Local_time'].day,
                    data_result['Local_time'].month,
                    data_result['Local_time'].year,
                    **data_result)

        ROOT.send_message(message.chat.id, text=result, parse_mode='HTML')
    else:
        ROOT.send_message(message.chat.id, 'Error '+response['cod'])
