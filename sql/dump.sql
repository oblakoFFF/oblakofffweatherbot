CREATE TABLE person (
	id integer PRIMARY KEY AUTOINCREMENT,
	user_id integer,
	location_x float,
	location_y float
);
